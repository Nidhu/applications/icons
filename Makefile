ICONS = *.svg
TARGET = /usr/share/nidhu/icons/

install:
	pacman -S papirus-icon-theme
	mkdir -p $(TARGET)
	mv $(ICONS) $(TARGET)
